<?php

return [

    'login_menu' => 'Přihlášení',
    'login' => 'Přihlásit',
    'email' => 'Email',
    'password' => 'Heslo',
    'remember' => 'Pamatovat si mně',
    'register_menu' => 'Registrace',
    'register' => 'Registrovat',
    'name' => 'Jméno',
    'confirm' => 'Potvrdit heslo',
    'logout' => 'Odhlásit se',
    'dashboard' => 'Úvod',
    'registered' => 'Registrace proběhla v pořádku. Nyní se můžete přihlásit.',
    'no_account' => 'Ještě nemáte účet?',
    'get_registered' => 'Zaregistrujte se.'

];
